-- Simple Lua-File-System
-- 2018 (c) kontakt@herrsch.de

fs = {}
fs.ENV = os.getenv("HOME")
fs.DOCUMENTS = fs.ENV.."/Documents"
fs.DROPBOX = fs.DOCUMENTS.."/Dropbox.assets"
--fs.PROJECT = fs.DOCUMENTS.."/"..fs.getProjectName()..".codea" -- TODO




-- This allows Lua to find and include files that are inside Dropbox and the current Project path
-- default package.path is /var/containers/Bundle/Application/AC80B6CE-8AEE-422D-8163-4B45033C2171/Codea.app/Frameworks/RuntimeKit.framework/?.lua
--
package.path = package.path..";"..fs.DROPBOX.."/?.lua"
--package.path = package.path..";"..fs.DROPBOX.."/?.lua;"..fs.PROJECT.."/?.lua"




local MIME = { -- a subset of available mime types
    [".text"] = "text/plain",
    [".txt"] = "text/plain",
    [".md"] = "text/markdown",
    [".markdown"] = "text/markdown",
    [".lua"] = "text/x-lua",
    [".luac"] = "application/x-lua-bytecode",
    [".pdf"] = "application/pdf",
    [".jpeg"] = "image/jpeg",
    [".jpg"] = "image/jpeg",
    [".gif"] = "image/gif",
    [".png"] = "image/png",
    [".tiff"] = "image/tiff",
    [".html"] = "text/html",
    [".htm"] = "text/html",
    [".css"] = "text/html",
    [".js"] = "application/javascript",
    [".json"] = "application/json",
    [".xml"] = "text/xml",
    [".plist"] = "application/x-plist"
}




-- Split string into directory, file name and extension
--
function fs.breadcrumbs(str)
    return string.match(str, "(.+)/(.+)(%.[^.]+)$")
end




function fs.read(file)
    local DIR, FILE, EXT = fs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "r")
    
    if data then
        local content = data:read("*all")
        data:close()
        return content, MIME[EXT]
    end
    
    return false
end




function fs.write(file, content)
    local DIR, FILE, EXT = fs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "w")
    
    if data then
        wFd:write(td)
        wFd:close()
        return true
    end
    
    return false
end




function fs.append(file, content)
    -- TODO: use for log files?
end




function fs.binaryRead(file)
    local DIR, FILE, EXT = fs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "rb")
    
    if data then
        local chunks = 512
        local content = ""
        
        while true do
            local bytes = data:read(chunks) -- read only n bytes per iteration
            if not bytes then break end
            content = content..bytes
        end
        
        data:close()
        
        return content, MIME[EXT]
    end
    
    return false
end




function fs.bineryWrite(file, content)
    local DIR, FILE, EXT = fs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "wb")
    
    if data then
        data:write(content) -- TODO write in chunks like lfs.binaryRead()?
        data:close()
        return true
    end
    
    return false
end




-- Finds the project Plist file and parses it
-- [@project] is a project name string or the complete path to the project.codea documents package
--
function fs.readPlist(project)
    project = project or fs.getProjectName()
    local package = string.match(project, ".+.codea$")
    local plist = package and string.format("%s/%s", package, "/Info.plist") or string.format("%s/%s.codea/Info.plist", fs.DOCUMENTS, project)
    local xml, mime = fs.read(plist)
    return parsePlist(xml)
end




-- Search and list available projects
-- [@collection] is a string name of a collection to restrict the search scope to
--
function fs.listProjects(collection)
    return listProjects(collection)
end




-- Searches all projects and compares the number of tabs and the Main.lua file to find this project's name
-- This is a workaround because Codea can not tell you the name of the current project
--
function fs.getProjectName()
    local projects = fs.listProjects()
    local this_tabs = listProjectTabs()
    local this_main = readProjectTab("Main")
    
    for id, name in ipairs(projects) do
        local project_tabs = listProjectTabs(name)
        local project_main = readProjectTab(string.format("%s:%s", name, "Main"))
        if #this_tabs == #project_tabs and this_main == project_main then
            return name
        end
    end
end




-- Get a file's or project's complete path
-- @file is a string name of the file (with extension) available in this project, e.g. "Info.plist" or "Main.lua"
-- [@project] is a string name of the project to search the file in, e.g. "Voxel Editor"
--
-- Alternatively you can pass only one parameter that is a typical Codea selector
-- with or without file extension e.g. "Voxel Editor:Main" or "Voxel Editor:Main.lua",
-- in case without any file extension, .lua is assumed
--
-- Alternativelly you can also pass one parameter that is a Codea project selector, e.g. "Documents:Voxel Editor" or "Dropbox:Spine"
-- where .codea is appended to projects in Documents but nothing to Dropbox because projects there are inside plain folders
--
-- If you pass only a string then that's assumed to be the project name you are searching for in Documents,
-- so that e.g "acme" becomes "Documents:acme"
--
-- Fallowing this convention you can also pass something like this "Dropbox:Spine/Bone.lua" to dive deeper into the folder structures
-- or append subfolder later inside require()
--
function fs.getPath(file, project)
    local pattern = fs.DOCUMENTS.."/%s.codea/%s" -- project, file, extension
    
    function hasExtension(f)
        return string.match(f, "%.[^.]+$") and true or false
    end
    
    if not project and file then -- e.g. ("Main.lua" or "Main"), nil
        if string.find(file, ":") then -- e.g. "Voxel Editor:Main", nil
            project, file = string.match(file, "(.+):(.+)") -- e.g. ("Documents:acme" or "Dropbox:Spine"), nil
            if project == "Documents" or project == "Dropbox" then
                if project == "Dropbox" then
                    pattern = fs.DROPBOX.."/%s/%s"
                end
                project, file = file, nil
            end
        end
    end
    
    project = project or fs.getProjectName()
    file = file and (hasExtension(file) and file or file..".lua") or ""
    return string.match(string.format(pattern, project, file), "(.+)/$") -- without trailing slash
end




-- TODO make method for getting project name from path
--
function fs.splitPath(str)
end



-- Find all files THIS project uses and list them
-- Include dependencies also!
-- This is a workaround because only the Plist file can tell which tabs (files) the project uses and which dependencies it has
-- (in turn, the files of those dependencies must be be listed too)
-- [@project] is a project name string or the complete path to the project.codea documents package
--
function fs.listProjectFiles(project)
    
    
    
    
    
    local project = fs.getProjectName()
    path = path or fs.getPath(project)
    local plist = fs.readPlist(path)
    local files = {[project] = plist["Buffer Order"]}
    
    -- TODO make recursive dependencies work and be output in nested format
    -- (right now only one level of dependencies work maybe because of mixed keys 0-9 and %l+)
    -- maybe autput as list of [Tab] = "/full/path/to/file.lua"
    
    --[[
    if plist.Dependencies then
        local proj = plist.Dependencies[1]
        --local name = string.match(proj, ".+:(.+)")
        local path = fs.getPath(proj)
        printf(fs.listProjectFiles(path))
    end
    --]]
    
    for id, dependency in ipairs(plist.Dependencies) do
        local name = string.match(dependency, ".+:(.+)")
        local dir = fs.getPath(name)
        print(name, dir)
        --files[name] = fs.listProjectFiles(dir)[name]
    end
    
    return files
end
