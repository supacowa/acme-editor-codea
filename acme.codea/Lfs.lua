-- Simple Lua-File-System
-- 2018 (c) kontakt@herrsch.de

lfs = {}

lfs.ENV = os.getenv("HOME")
lfs.DOCUMENTS = lfs.ENV.."/Documents"
lfs.DROPBOX = lfs.DOCUMENTS.."/Dropbox.assets"

-- extend the require scope
-- default package.path is /var/containers/Bundle/Application/AC80B6CE-8AEE-422D-8163-4B45033C2171/Codea.app/Frameworks/RuntimeKit.framework/?.lua
package.path = package.path..";"..lfs.DROPBOX.."/?.lua"

local MIME = { -- a subset of available mime types
    [".text"] = "text/plain",
    [".txt"] = "text/plain",
    [".md"] = "text/markdown",
    [".markdown"] = "text/markdown",
    [".lua"] = "text/x-lua",
    [".luac"] = "application/x-lua-bytecode",
    [".pdf"] = "application/pdf",
    [".jpeg"] = "image/jpeg",
    [".jpg"] = "image/jpeg",
    [".gif"] = "image/gif",
    [".png"] = "image/png",
    [".tiff"] = "image/tiff",
    [".html"] = "text/html",
    [".htm"] = "text/html",
    [".css"] = "text/html",
    [".js"] = "application/javascript",
    [".json"] = "application/json",
    [".xml"] = "text/xml",
    [".plist"] = "application/x-plist"
}


function lfs.breadcrumbs(path)
    return path:match("(.+)/(.+)(%.[^.]+)$") -- split into directory, file name and extension
end


function lfs.read(file)
    local DIR, FILE, EXT = lfs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "r")
    
    if data then
        local content = data:read("*all")
        data:close()
        return content, MIME[EXT]
    end
    
    return false
end


function lfs.write(file, content)
    local DIR, FILE, EXT = lfs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "w")
    
    if data then
        wFd:write(td)
        wFd:close()
        return true
    end
    
    return false
end


function lfs.append(file, content)
    -- TODO: use for log files?
end


function lfs.binaryRead(file)
    local DIR, FILE, EXT = lfs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "rb")
    
    if data then
        local chunks = 512
        local content = ""
        
        while true do
            local bytes = data:read(chunks) -- read only n bytes per iteration
            if not bytes then break end
            content = content..bytes
        end
        
        data:close()
        
        return content, MIME[EXT]
    end
    
    return false
end


function lfs.bineryWrite(file, content)
    local DIR, FILE, EXT = lfs.breadcrumbs(file)
    local data = io.open(string.format("%s/%s", DIR, FILE..EXT), "wb")
    
    if data then
        data:write(content) -- TODO write in chunks like lfs.binaryRead()?
        data:close()
        return true
    end
    
    return false
end


function lfs.listProjects(collection)
    local projects = listProjects(collection)
    local this_tabs = listProjectTabs()
    
    for id, name in ipairs(projects) do
        local project_tabs = listProjectTabs(name)
        local project_content = readProjectTab(string.format("%s:%s", name, "Main"))
        local this_content = readProjectTab("Main")
        
        if #this_tabs == #project_tabs and this_content == project_content then
            return projects, name
        end
    end
    
    return projects
end


function lfs.readPlist(project)
    local plist = string.format("%s/%s.codea/Info.plist", lfs.DOCUMENTS, project)
    local xml, mime = lfs.read(plist)
    return parsePlist(xml), mime, plist
end
