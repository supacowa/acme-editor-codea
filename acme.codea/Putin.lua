Putin = {}

function Putin:open()
    self.files = listProjectTabs()
end


function Putin:create(file, content)
    -- you cant actually as for name because maybe we just create a view in memory and will close it without saving anyway
    saveProjectTab(file, content)
    table.insert(self.files, file)
end


function Putin:delete(file)
    saveProjectTab(file, nil)
end


function Putin:close(file)
    --if closing file that is empty then call self:delte(file)
end
